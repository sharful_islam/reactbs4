import React, { Component } from 'react';
import './App.css';

import AppContent from'./AppContent';

export class App extends Component{
  
  render(){
    return (
      <div className="App">
        <AppContent className="ml-auto d-flex" />
      </div>
    );
  }
}

export default App;
