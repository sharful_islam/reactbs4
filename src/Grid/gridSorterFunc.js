import FuzzySearch from 'fuzzy-search';
import fsJson from './gridData';

// code is wrapped within a (function(){... })(); // self-executing no window load required 
(function(){

const fsHeaders = {
  "name": "Name",
  "type": "Type",
  "modified": "Date Modified",
  "size": "Size"
};
/*
<UI>
	<domHeaderItems>
		<searchAssets onclick={} onkeyup={}>searchText</searchAssets>
	</domHeaderItems>
	<domTbodyItems fsJson={fsJson} selectAll={} searchFilter={fsFilter}
</UI>

*/
let fsFilter = []; // no filters applied yet
let selectAll;
let sortKey;
let searchAssets;
let domTbodyItems;
let domHeaderItems;

prepareData(); // add id, selected and hidden

const $container = document.getElementById('fsTableArea');
init();

// begin
function init(){
	document.addEventListener("DOMContentLoaded", function(event) {
		$container.innerHTML="loading...";
		initSearchFilter();
		initView();
	});
}
// helpers

function getDomElementById(elId){
	return (document.getElementById(elId));
}

function getCurrentDomsByTag(tag){
	return document.getElementsByTagName(tag);
}

function getCurrentData(){
	// applies filter when required
	let j = fsJson;
	if (fsFilter.length>0){
		// return filtered data when filtered
		j = fsJson.filter(function(data, key){
			// returns filtered data result
			return fsFilter.includes(data['id']);
		})
	}
	if (sortKey){
		return j.sort((a, b) => (a[sortKey] >= b[sortKey]) ? 1 : -1);
	}
	return(j);
}
function initSearchFilter(){
  	searchAssets = getDomElementById('searchAssets'); // searchBox

  	searchAssets.addEventListener('click', function(e){
  		// x btn - if there is no text in the searchbox, want to clear the filter
  		setTimeout(()=> (!searchAssets.value ? resetFilter():''), 200);
  	});

  	searchAssets.addEventListener('keyup', function(e){
  		// checks all keyUp events on the searchBox
		const searcher = new FuzzySearch(fsJson, Object.keys(fsHeaders), {
		  caseSensitive: false,
		});
		const result = searcher.search(searchAssets.value);
		setFilter(result);
		initView();
  	})
}

function prepareData(){
	// initialize data - add id, checked
	selectAll = false; //initial value
	getKeys(fsJson).map(function(idx){
		let foo = fsJson[idx];
		foo.id = idx;
		foo.checked = false;
		return true;
	})
}

// filter reset on click
function resetFilter(){
	fsFilter = [];
	initView();
}

// setup filters 
function setFilter(f) {
	// adjust filter based on search results
	fsFilter = f.map(function(i, k){
		// get the IDs of the available data (all else should be hidden)
		return(i['id']);
	});
}

function getFieldDomElm(fld, id, val){
	let typ = (fld === 'checked') ? 'input':'div';
	let div;
	let existing = getDomElementById(id);
	if (existing) existing.remove();


	if (typ==='input') {
			div = getCheckBox(id, fld, val);
			div.checked = val;
			div.className="pull-left w-5 d-block";
	}
	else {
		if (existing) existing.remove();
		div=getNewDomElem(typ)
		div.setAttribute('id', id);
		div.setAttribute('hidden', (typ==='hidden'))
		div.hidden = (typ==='hidden');
		

		if (typ==='input'){
			div.setAttribute('type', 'checkbox')
			div.checked = (val===true);
		} else {
			div.className='d-table-cell col bg-light h6';
			div.append(val)
		}

	}
	return(div)
}

// initialize the view
function initView(){
 	let c = getDomElementById('fsTableArea');
	let newEl = getNewDomElem('div');
		newEl.setAttribute('id', 'fsTableArea'); // replacement data

	let header = getHeaderRowElement(getNewDomElem('div'))
		header.className='navbar bg-light navbar-fluid ';

	newEl.appendChild(header);
	
	let rows = getRowElements(getNewDomElem('div'));			

	newEl.appendChild(rows);

	c.parentNode.replaceChild(newEl, c);
}

function setDataRowSelection(id, value){
	let d = getDomElementById('checked-'+id);
	console.warn(id, value);

	Array.prototype.filter.call(fsJson, function(val, key){
		if (val.id === id){
			val.checked = (value===true);
			console.warn(getDomElementById('checked-'+id));
		}
	});
	if(d){
		d.checked = (value===true);
		console.warn(d);
	}
}

function getRowElements(newEl){
	let data = getCurrentData();
	Array.prototype.filter.call(data, function(itemObj){
		let row = getNewDomElem('div');
		row.className='d-flex container-fluid';
		getKeys(itemObj).forEach(function(fld) {
			// set the divs for each row
			let id = fld+'-'+itemObj['id'];
			let div = getFieldDomElm(fld, id, itemObj[fld]);

			if (getKeys(fsHeaders).includes(fld)){
				row.appendChild(div)
			} else
				if (fld==='checked'){
					row.prepend(div);
				}
			/*if (getKeys(fsHeaders).includes(fld)){
				row.appendChild(div)
			} else {
				if (fld==='checked'){
					console.warn(fld, id, itemObj[fld]);
					let x = (getCheckBox(id, fld, itemObj[fld]))
					x.addEventListener('change',(e)=> setDataRowSelection(e.target.id.split('-')[1], e.target.checked))
					x.checked=itemObj[fld].checked;
					row.prepend(x);
				}
			}*/
		})
		newEl.appendChild(row);
	})
	domTbodyItems= newEl;

	return newEl
}
function getKeys(o){
 return (Object.keys(o))
}

function getNewDomElem(e){

	var el = (document.createElement(e));
	return (el);
}

function changeSortOrder(evt){
	if (sortKey === evt.target.id){
		evt.stopPropagation();
		evt.preventDefault();
		return;
	} 
  	sortKey = evt.target.id; // key is the field
  	initView();
}

function getCheckBox(elId, tagName, val){
	// bind template to data
	let existing = getDomElementById(elId);
	if (existing){
		existing.setAttribute('checked', val)
		// set value and return existing element
		return(existing.parentElement);
	} 

	var tX = getNewDomElem(tagName);
		tX.className='m-auto pl-2 mt-2';

	var ck = getNewDomElem('input');
		ck.className='w-5 ml-3'; // fixme: map widths
		ck.setAttribute('type', 'checkbox');
		ck.setAttribute('checked', val);
		ck.addEventListener('change', (evt)=>checkClicked(evt, elId));
		ck.setAttribute('id', elId);
	tX.appendChild(ck);
	// console.warn(tX); //
	return (tX);
}


function checkClicked(e){
	if(e.target.id==='check-all'){
		// update all visible checkboxes - since we may have filter applied, we don't clear the rest
		selectAll = e.target.checked;
		getCurrentData().forEach((i)=>setDataRowSelection(i, e.target.checked));
		initView();

		let dData = getCurrentDomsByTag('input');
		let replaceVals = e.target.checked;

		getKeys(dData).forEach(function(checkbox){
			// update DOM checkboxes
			dData[checkbox].checked = replaceVals;
		});
	
	}
}

function getHeaderRowElement(el) { // <thead ...>
	//el.className='w-100';
	let checkBox = getCheckBox('check-all', 'div', selectAll); // check-all checkbox 
		checkBox.className='w-5'
	el.appendChild(checkBox);

	getKeys(fsHeaders).forEach(function(hd) {
		var e2 = (getNewDomElem('div'));
			e2.setAttribute('id', hd);
			//e2.bind(hd);
		e2.className='w-23 h6';
		e2.addEventListener('click', (evt)=>changeSortOrder(evt));

		e2.append(fsHeaders[hd]);
		el.appendChild(e2);
	});
	domHeaderItems = el;

	return (el);
}


function getDataRow(id){
	// get a row of data by id
	return fsJson.filter((d)=>d.id==id)[0];
}


})();