import React, { Component } from 'react';
import * as AppIcons from 'react-icons/fa';

export class SideBarList extends Component{
  constructor(props){
  	super(props);
  	this.state={
  		active: true
  	}
  	this.getNavItem.bind(this);
  }
  getNavItem(){
  	const navItem = this.props.items.map((item)=> {

	  	return (
	            <li className="nav-item " key={item.id}>
	              <a className="nav-link" href="#">
	                <i className="icon icon-lg m-1 p-1" ><AppIcons.FaCamera /></i> 
	                {item.title}
	              </a>
	             </li>
	    )
  	})
  	return (navItem);
  }

  render(){
  	return(this.getNavItem())
  }
}

export default SideBarList;
