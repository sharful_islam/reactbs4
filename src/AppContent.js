import React, { Component } from 'react';
import './App.css';

export class AppContent extends Component{
  constructor(props){
    super(props); // files, headers
    this.state = {
      sorted: false
    }

  }
  
  componentDidMount() {

      console.log('Component DID Mount!');
  }
  
  render(){
    return (
      <div className="App-content">
          <div className="w-100 row">
            <div className="btn-group ml-auto mr-5">
              <button type="button" className="btn bg-default ml-auto dropdown-toggle" data-toggle="dropdown">
                 Mr. Eazi
              </button>
              <div className="dropdown-menu">
                <a className="dropdown-item mb-0 mt-0 pt-0 pb-0" href="#">My Profile</a>
                <hr />
                <a className="dropdown-item mb-0" href="#">Notifications</a>
              </div>
            </div>
          </div>
          <div className="w-100">
            <h3>Files</h3>
            <div className="card card-light w-100">
              <div className="card-header w-100">
                <h4>Root / </h4>
              </div>
              <div className="card-body w-100">
                <button className="mr-3 btn btn-default bg-grey">
                    <i className="fa fa-plus mr-1"></i>New Folder</button>
                <button className="mr-3 btn btn-default bg-grey w-10">
                    <i className="fa fa-check mr-1"></i>Upload</button>
                <div className="Search-Box">
                   <form className="form-inline">
                      <input className="form-control " type="search" placeholder="Search..." aria-label="Search"  id="searchAssets" />
                    </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}

export default AppContent;
