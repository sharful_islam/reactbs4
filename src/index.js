import React from 'react';
import ReactDOM from 'react-dom';
// The jQuery imported here was to make the dropdowns on the top-right dropdown
import 'jquery';
// Out of the scss frameworks out there, Bootstrap 4 (not any older ones) to me seems optimal in producing almost any layout solutions amicably
import 'bootstrap';

import './index.css';
import App from './App';
import AppSidebar from'./SideBar/AppSidebar';

import './Grid/gridSorterFunc'; // vanilla js

import * as serviceWorker from './serviceWorker';

ReactDOM.render(<AppSidebar />, document.getElementById('sidebar'));
ReactDOM.render(<App />, document.getElementById('topright'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
