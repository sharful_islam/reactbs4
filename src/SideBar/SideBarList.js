import React, { Component } from 'react';

export class SideBarList extends Component{
  constructor(props){
  	super(props);
  	this.state={
  		active: true
  	}
  	this.getNavItem.bind(this);
  }
  getNavItem(){
  	const navItem = this.props.items.map((item)=> {

	  	return (
	            <li className="nav-item h5" key={item.id}>
	              <a className="nav-link" href={item.link}>
	                <i className={item.icon + ' p-2 mr-2 fa fa-md '} ></i>
	                <span className="">{item.title}</span>
	              </a>
	             </li>
	    )
  	})
  	return (navItem);
  }

  render(){
  	return(this.getNavItem())
  }
}

export default SideBarList;
