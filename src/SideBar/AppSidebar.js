import React, { Component } from 'react';

import logo from '../logo.svg';

import './AppSidebar.css';
import SideBarList from './SideBarList';

export class AppSidebar extends Component{
  constructor(props){
    super(props);
    this.state = {
        navItems:[
          { id: 0, link: '#jobs', title: 'Jobs', icon: ' fa-check'},
          { id: 1, link: '#files', title: 'Files', icon: ' fa-camera'},
          { id: 2, link: '#workflows', title: 'Workflows', icon: ' fa-home'},
          { id: 3, link: '#presets', title: 'Presets', icon: ' fa-cog'},
          { id: 4, link: '#software', title: 'Software', icon: ' fa-camera'},
          { id: 5, link: '#usage', title: 'Usage', icon: 'fa-cog'},
          { id: 6, link: '#connections', title: 'Connections', icon: 'fa-home'},
          { id: 7, link: '#desktop', title: 'Desktop', icon: 'fa-cog'}
        ]
    }
  }
  render(){
    return (
      <div>
        <nav className="App-sidebar navbar navbar-vertical bg-dark navbar-dark" role="toolbar">
          <ul className="navbar-nav w-100 bg-dark">
            <li className="nav-item">
              <img src={logo} className="App-logo m-0 p-0" alt="logo" width="50" /> Platform Logo
            </li>
            <li className="nav-item">
              <img src={logo} className="App-logo m-0 p-0" alt="logo" width="50" /> Client Logo
            </li>
          </ul>
          <br/>
          <ul className="navbar-nav w-100">
            <SideBarList items={this.state.navItems} />
          </ul>
          <div className="w-100 mt-auto">  
            <ul className="navbar-nav w-100 bg-dark">
              <li className="nav-item h6">
                <i className="fa fa-check"></i>
                Contact Us
              </li>
              <li className="copy text-sm">&copy; 2019 Confidential Company Name, &reg;</li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default AppSidebar;
