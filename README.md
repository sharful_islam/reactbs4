## React Bs4 Sample
This project was bootstrapped with [Create React App|https://github.com/facebook/create-react-app].
The Objective was to produce an integrated React App with one section that's written in pure js.

## Getting Started

In the Workspace directory, run the following to clone the project locally:

`git clone https://bitbucket.org/sharful_islam/reactbs4`

This clones the project into `reactbs4` directory.

`cd reactbs4`

Install the dependencies (I'm using yarn, but npm works just as well)

`yarn`
Or
`npm install`
 

## Initial Setup and Branches

Being new to React, I ran the following to generate initial project:

`create-react-app <project_name>`

Once the project is running locally, the different branches can be viewed locally simply by switching to the brances from the project folder (`-b` option below creates a new local branch)

`git checkout -b <branchname>`

Currently available branches are `polisher1` and `onewaybind`

The view in `localhost:3000` will auto-update (if running) to the branch selected.

### Layouts
 I opted to try and not use a css framework eventually opting to implement Bootstrap4 to enable a faster layout creation. The Bootstrap JavaScript was not mandatory to implement - this was adopted to use the JavaScript dropdowns for the top-right section for the User's menu.

### Icons
I discovered that Font-Awesome could not be imported in directly but required a wrapper. The alternate option was to import the Font-Awesome CSS from a CDN to successfully import the svg paths for the icons. This allows for flexibility in only adding in the required CSS for the icon within React allowing for a more flexible iconographic solution for the entire app.

### React Components
Although new to React, this was the least challenging part of the implementation. The most challenging issue in this respect was to add a secondary Component to load the left-side Navigation list-items dynamically - which required a binding to self. It was refreshing to experience the simplicity and straightforward and modular approach to front-end development. The next attempt for myself may be to externalize each component into it's own repository and modify `package.json` to point to the git repository url (seamlessly supported) thus making it similar to a 3rd party component.


### Sidebar
The left sidebar is comprised of 3 sections for the Logos, the Navigation and the Footer of the navigation in a fixed positioned contaiter. Bootstrap 4 allows for inline classes that correspond to common CSS classes such as `mr-3` translating to `margin-right: 3px` - or `w-100` for `width: 100%` but additional CSS amendments were required to ensure responsive layout adjustments and to ensure the `fixed` nature of it.

### DOM Rendering
After having set up the layout and upon attempting to insert plain JavaScript into the desired container, I discovered that the content was not being rendered. Then I discovered that React replaces the DOM element uplon rendering - overwriting the contents of the container. Research showed that the container required to be outside a React-rendered component to be able to circumvent this issue. The solution required substantial layout amendments to accommodate.

### 3rd party JavaScript
For Search, I used `FuzzySearch` npm package. [https://www.npmjs.com/package/fuzzy-search].
This was the only additional package used. 

It allowed me to define the fields to be searched and a subset of the JSON data returned based on search. the Search was adopted from a Bootstrap 4 sample. 

For the integration, I had to implement Event Listeners on-load of DOM to watch for changes within it and trigger the search. An additional event to clear the search had to be implemented with a Timeout for the UI to detect the change. 

### Plain JavaScript Data Manipulation
This was the first time I attempted to mimic an SPA with plain JavaScript. Typical use-case in front-end has always been at least at the level of `jQuery`. 

#### Helpers don't help
Helpers such as `_` (underscore/loDash) typically mask the `Object` and `Array` methods for Object and Array manipulations respectively. This excercise was at the flip-side of it - compelling me to acclamate my understanding and usage thereof. 

#### UI Interactions - Search, Sort, Select
Once the UI was integrated and the data inserted into view, the Objectives were to format it into a consistent layout with the rest of the app and include functionalities `sort`, `search` and `select` with plain JS. 

While visually the entire App looked as expected, there was little I could do without a major rewrite as after the initial implementation of Sort and Search, Select became more challenging. I created a new branch to address the issue separately ([Branch Link|https://bitbucket.org/sharful_islam/reactbs4/src/polisher1]). 

Once I added functions to manipulate the DOM directly with the data available from the JSON, the implementation compromised the Sorting, directing my attention to the reasons behind the creation of React and other front-end languages in the first place. 

The rework to bypass this issue included ensuring each dynamic data container had an ID (for sorting) with the Attribute name attached to it; replacing the entire container with a `div` instead of a `table`; reworking the header for sorting; and consolidating all 3 into a `currentData` method to be assessed each time.

This is put into a separate branch and not yet merged into master ([Branch Link | https://bitbucket.org/sharful_islam/reactbs4/src/onewaybind]) as some styling adjustments are deemed necessary.

## Available Scripts

In the project directory, you can run (`yarn` can be substituted for `npm` below):

### `yarn run start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn run test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
